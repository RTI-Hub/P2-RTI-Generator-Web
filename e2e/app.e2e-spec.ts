import { P2RTIGeneratorWebPage } from './app.po';

describe('p2-rti-generator-web App', () => {
  let page: P2RTIGeneratorWebPage;

  beforeEach(() => {
    page = new P2RTIGeneratorWebPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
