import { browser, by, element } from 'protractor';

export class P2RTIGeneratorWebPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }
}
